from flask import Blueprint, render_template, redirect
from docker import DockerClient

try:
    con = DockerClient('tcp://127.0.0.1:2375')
except Exception as e:
    print('Erro: {}'.format(e))

docker = Blueprint('docker', __name__, url_prefix="/docker")


@docker.route('')
def index():
    containers = con.containers.list(all=True)
    return render_template('newdocker.html', ctrs=containers)


@docker.route('/start/<string:id>')
def start(id):
    con.containers.get(id).start()
    return redirect('/docker')


@docker.route('/stop/<string:id>')
def stop(id):
    con.containers.get(id).stop()
    return redirect('/docker')
