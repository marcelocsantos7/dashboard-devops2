from flask import Blueprint, render_template, redirect, request
from jenkins import Jenkins


jks = Blueprint('jenkins', __name__, url_prefix="/jenkins")
try:
    con = Jenkins("http://localhost:8080", username="marcelo", password="123456")
except Exception as e:
    print(e)


@jks.route('')
def index():
    return render_template('jenkins.html', jobs=con.get_all_jobs())


@jks.route('/build/<job>')
def build_job(job):
    con.build_job(job)
    return redirect('/jenkins')


@jks.route('/update/<job>')
def update_job(job):
    return render_template('jenkins_update.html', job=job, xml=con.get_job_config(job))


@jks.route('/reconfig/<job>', methods=['POST'])
def reconfig_job(job):
    con.reconfig_job(job, request.form['xml'])
    return redirect('/jenkins')
